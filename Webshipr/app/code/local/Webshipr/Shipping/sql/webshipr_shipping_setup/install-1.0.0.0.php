<?php
$installer = $this;
/* @var $installer Mage_Sales_Model_Resource_Setup */

$installer->startSetup();

$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_quote')}` ADD `pickup_location_id` VARCHAR(255) COMMENT 'pickup_location_id'");
$installer->run("ALTER TABLE `{$installer->getTable('sales_flat_order')}` ADD `pickup_location_id` VARCHAR(255) COMMENT 'pickup_location_id'");

$installer->endSetup();
