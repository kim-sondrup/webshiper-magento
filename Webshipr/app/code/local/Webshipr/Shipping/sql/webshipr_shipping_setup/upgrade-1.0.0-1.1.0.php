<?php
/**
 * @var Mage_Sales_Model_Resource_Setup $installer
 * @var Varien_Db_Adapter_Interface     $conn
 */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('order', 'webshipr_tracking_url', array('type' => 'varchar'));

$installer->endSetup();
