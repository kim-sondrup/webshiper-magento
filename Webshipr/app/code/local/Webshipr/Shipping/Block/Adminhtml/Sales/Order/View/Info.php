<?php
require_once('Webshipr/Shipping/Model/WebshiprGlue.php');

class Webshipr_Shipping_Block_Adminhtml_Sales_Order_View_info extends Mage_Adminhtml_Block_Sales_Order_View_Info
{

    protected function _beforeToHtml()
    {
        //use only for oder view page
        $handles = $this->getLayout()->getUpdate()->getHandles();
        if (
            !in_array('adminhtml_sales_order_shipment_new', $handles) &&
            !in_array('adminhtml_sales_order_shipment_view', $handles) &&
            !in_array('adminhtml_sales_order_invoice_new', $handles) &&
            !in_array('adminhtml_sales_order_invoice_view', $handles) &&
            !in_array('adminhtml_sales_order_creditmemo_new', $handles) &&
            !in_array('adminhtml_sales_order_creditmemo_view', $handles)
        ) {
            $this->addWebshiprInfo();
        }
        parent::_beforeToHtml();
    }

    protected function addWebshiprInfo()
    {
        /** @var Webshipr_Shipping_Model_WebshiprGlue $glue */
        $glue = Mage::getModel('webshipr_shipping/webshiprGlue');
        $order = $this->getOrder();
        $webshiprMethod = $this->getRequest()->getParam('webshipr_method');
        $rateId = $this->getRequest()->getParam('rate_id');
        $checkOld = false;

        switch ($webshiprMethod) {
            case 'process':
                $glue->processOrder($order, $rateId);
                break;
            case 'Reprocess':

                $glue->reProcessOrder($order, $rateId);
                break;
        }

        echo '<div class="webshipper-block">';
        echo '<div class="entry-edit-head">';
        echo '<h4>Webshipr</h4>';
        echo '</div>';
        echo '<div class="fieldset">';

        // Check if order is in webshipr
        $data = $this->getOrder()->getData();
        $orderId = $data["increment_id"];
        $entityId = $this->getOrder()->getId();

        if ($this->getAPI()->orderExists($orderId)) {
            $webshiprOrder = $this->getAPI()->getOrder($orderId);

            // Check if the status of order requires reprocessing
            if (
            in_array(
                $webshiprOrder->status,
                array(
                    'dispatched',
                    'pending_partner',
                    'partner_processing',
                    'partner_waiting_for_stock',
                    'success'
                )
            )
            ) {
                // Seems to be processed correctly.
                $this->writeProcessed($webshiprOrder);
                $this->writeReprocess1($webshiprOrder);
            } else {
                // Something went wrong. Allow to reprocess.
                $this->writeReprocess($webshiprOrder);
            }
        } else if ($checkOld && $this->getAPI()->orderExists($entityId)) {
            $webshiprOrder = $this->getAPI()->getOrder($entityId);

            // Check if the status of order requires reprocessing
            if (
            in_array(
                $webshiprOrder->status,
                array(
                    'dispatched',
                    'pending_partner',
                    'partner_processing',
                    'partner_waiting_for_stock',
                    'success'
                )
            )
            ) {
                // Seems to be processed correctly.
                $this->writeProcessed($webshiprOrder);
                $this->writeReprocess1($webshiprOrder);
            } else {
                // Something went wrong. Allow to reprocess.
                $this->writeReprocess($webshiprOrder);
            }
        } else {
            $this->writeProcess();
        }


        echo '</div>';
        echo '</div>';
    }

    protected function writeProcessed($webshiprOrder)
    {
        echo '
        <table class="form-list" cellspacing="0">
                <tbody>
                <tr>
                    <td class="label">' . $this->__('Order status') . '</td>
                    <td>' . $this->writeStatus($webshiprOrder->status) . '</td>
                </tr>
                ';
        if (strlen($webshiprOrder->tracking_url) > 1) {
            echo '<tr>
                        <td class="label">' . $this->__('Tracking url') . '</td>
                        <td><a href="' . $webshiprOrder->tracking_url . '" target="_blank">' . $this->__('Click here') . '</a></td>
                    </tr>';
        }
        if (strlen($webshiprOrder->print_link) > 1) {
            echo '<tr>
                <td class="label">' . $this->__('Label url') . '</td>
                <td><a href="' . $webshiprOrder->print_link . '" target="_blank">' . $this->__('Click here') . '</a></td>
            </tr>';
        }
        if (strlen($webshiprOrder->print_return_link) > 1) {
            echo '<tr>
                <td class="label">' . $this->__('Label url') . '</td>
                <td><a href="' . $webshiprOrder->print_return_link . '" target="_blank">' . $this->__('Click here') . '</a></td>
            </tr>';
        }

        echo '</tbody></table>';
    }

    protected function writeProcess()
    {
        echo '
            <form method="get">
                <input type="hidden" name="webshipr_method" value="process">
                <table class="form-list" cellspacing="0">
                        <tbody>
                        <tr>
                            <td class="label">' . $this->__('Order status') . '</td>
                            <td><b>' . $this->__('Not sent to webshipr yet') . '</b></td>
                        </tr>
                        <tr>
                            <td class="label">' . $this->__('Process') . '</td>
                            <td>';
        echo $this->getRatesDropDown();
        echo '<input type="submit" value="' . $this->__('Process') . '"></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        ';
    }

    protected function writeReprocess($webshiprOrder)
    {

        $shipmentErrors = $webshiprOrder->shipment_errors;
        $errormsg = '';
        foreach ($shipmentErrors as $error) {

            $errormsg .= $error->error_message . "<br/>";
        }

        echo '
            <form method="get">
                <input type="hidden" name="webshipr_method" value="' . $this->__('Reprocess') . '">
                <table class="form-list" cellspacing="0">
                        <tbody>
                        <tr>
                            <td class="label">
                                ' . $this->__('Order status') . '
                            </td>
                            <td>';
        echo $this->writeStatus($webshiprOrder->status, $errormsg);
        echo '</td>
                        </tr>
                        <tr>
                            <td class="label">
                                ' . $this->__('Reprocess') . '</td>
                            <td>';
        echo $this->getRatesDropDown();
        echo '<input type="submit" value="' . $this->__('Reprocess') . '"></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        ';
    }

    protected function writeReprocess1($webshiprOrder)
    {

        $shipmentErrors = $webshiprOrder->shipment_errors;
        $errormsg = '';
        foreach ($shipmentErrors as $error) {

            $errormsg .= $error->error_message . "<br/>";
        }


        echo '
            <form method="get">
                <input type="hidden" name="webshipr_method" value="' . $this->__('Reprocess') . '">
                <table class="form-list" cellspacing="0">
                        <tbody>

                        <tr>
                            <td class="label">
                                ' . $this->__('Reprocess') . '</td>
                            <td>';
        echo $this->getRatesDropDown();
        echo '<input type="submit" value="' . $this->__('Reprocess') . '"></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        ';
    }

    protected function getRatesDropDown()
    {
        $order = $this->getOrder();
        $glue = Mage::getModel('webshipr_shipping/webshiprGlue');
        $rateId = 0;
        if ($glue->isWebshiprRate($order["shipping_method"])) {
            $rateId = $glue->getWebshiprRateIDFromMethod($order["shipping_method"]);
        }
        echo "<select name=\"rate_id\">";
        foreach ($this->getAPI()->getShippingRates() as $rate) {
            echo "<option value=\"" . $rate->id . "\" " . ((int)$rateId == (int)$rate->id ? "selected" : "") . ">" . $rate->name . "</option>";
        }
        echo "</select>";
    }

    /**
     * @return Webshipr_Shipping_Model_WebshiprAPI
     */
    protected function getAPI()
    {
        /** @var Webshipr_Shipping_Model_WebshiprAPI $apiModel */
        $apiModel = Mage::getModel('webshipr_shipping/webshiprAPI');
        $apiModel->init($this->getOrder()->getStore());
        return $apiModel;
    }

    protected function writeStatus($status, $errormsg)
    {
        switch ($status) {
            case "not_recognized":
                return "<font style='color: red;'>" . $this->__('Not recognized') . "</font><br/> <i>" . $errormsg . "</i>";
                break;
            case "country_rejected":
                return "<font style='color: yellow;'>" . $this->__('Country rejected') . "</font><br/> <i>" . $errormsg . "</i>";
                break;
            case "dispatched":
                return "<font style='color: green; font-weight: bold;'>" . $this->__('Sent to shipper') . "</font>";
                break;
            case "carrier_error":
                return "<font style='color: red; font-weight: bold;'>" . $this->__('Carrier error') . "</font><br/> <i>" . $errormsg . "</i>";
                break;
            case "error_processing":
                return "<font style='color: red; font-weight: bold;'>" . $this->__('Carrier error') . "</font><br/> <i>" . $errormsg . "</i>";
                break;
            case "disabled":
                return "<font style='color: red; font-weight: bold;'>" . $this->__('Subscription disabled') . "</font> <i> " . $errormsg . " </i>";
                break;
            case "not_processed":
                return "<font style='color: red; font-weight: bold;'>" . $this->__('Not processed') . "</font> <i> " . $errormsg . " </i>";
                break;
            case "limit_exceeded":
                return "<font style='color: red; font-weight: bold;'>" . $this->__('Limit exceeded') . "</font> <i> " . $errormsg . " </i>";
                break;
            case "partner_processing":
                return "<font style='color: blue; font-weight: bold;'>" . $this->__('Partner processing') . "</font> <i> " . $errormsg . " </i>";
                break;
            case "pending_partner":
                return "<font style='color: blue; font-weight: bold;'>" . $this->__('Pending partner') . "</font> <i> " . $errormsg . " </i>";
                break;
            case "partner_waiting_for_stock":
                return "<font style='color: yellow; font-weight: bold;'>" . $this->__('Partner waiting for stock') . "</font> <i> " . $errormsg . " </i>";
                break;
            case "partly_dispatched":
                return "<font style='color: yellow; font-weight: bold;'>" . $this->__('Partly dispatched') . "</font> <i> " . $errormsg . " </i>";
                break;
            default:
                return $status;
                break;
        }


    }
}
