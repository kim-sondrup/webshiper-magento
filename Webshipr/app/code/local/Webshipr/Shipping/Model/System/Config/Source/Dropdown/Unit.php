<?php

class Webshipr_Shipping_Model_System_Config_Source_Dropdown_Unit
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => '1000',
                'label' => 'Gram',
            ),
            array(
                'value' => '1',
                'label' => 'KG',
            ),
        );
    }
}
