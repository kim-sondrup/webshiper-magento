<?php

class Webshipr_Shipping_Model_Quote_Address_Rate extends Mage_Sales_Model_Quote_Address_Rate
{
    public function importShippingRate(Mage_Shipping_Model_Rate_Result_Abstract $rate)
    {
        parent::importShippingRate($rate);
        if (!($rate instanceof Mage_Shipping_Model_Rate_Result_Error)) {
            $this->setDynamicPickup($rate->getDynamicPickup());
            $this->setWsRateId($rate->getWsRateId());
        }

        return $this;
    }
}
